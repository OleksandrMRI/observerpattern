package ua.shpp.observer;

import ua.shpp.abstracts.Observable;
import ua.shpp.classes.ConsoleObserver;
import ua.shpp.classes.FileObserver;
import ua.shpp.classes.MeteoStation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class AppObserver {
    public static void main(String[] args) {
        Observable observable = new MeteoStation();
        observable.addObserver(new ConsoleObserver());

        try (FileWriter fileWriter = new FileWriter("FileObserver", StandardCharsets.UTF_8)) {
            observable.addObserver(new FileObserver(fileWriter));

            observable.setMeasurements(12, 750);
            observable.setMeasurements(0, 755);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}