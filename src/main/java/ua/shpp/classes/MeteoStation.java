package ua.shpp.classes;

import ua.shpp.abstracts.Observable;
import ua.shpp.abstracts.Observer;

import java.util.ArrayList;
import java.util.List;

public class MeteoStation implements Observable {
    int temperature;
    int pressure;
    List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void setMeasurements(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
        notifyObservers();
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.handleEvent(temperature, pressure);
        }
    }
}
