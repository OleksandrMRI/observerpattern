package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.Observer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileObserver implements Observer {
    FileWriter fileWriter;
    Logger log = LoggerFactory.getLogger(FileObserver.class);

    public FileObserver(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    public void handleEvent(int temperature, int pressure) {

        try {
            fileWriter.append("The weather has changed. Writing to file. Temperature is " + temperature + ". " +
                    "Pressure is " + pressure + ".\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
