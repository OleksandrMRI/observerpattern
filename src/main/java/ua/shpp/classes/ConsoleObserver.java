package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.Observer;

public class ConsoleObserver implements Observer {
    Logger log = LoggerFactory.getLogger(ConsoleObserver.class);

    public void handleEvent(int temperature, int pressure) {
        log.info("The weather has changed. Writing to console. Temperature is {}. Pressure is {}", temperature, pressure);
    }
}
