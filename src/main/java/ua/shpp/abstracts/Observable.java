package ua.shpp.abstracts;

public interface Observable {
    void addObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();

    void setMeasurements(int temperature, int pressure);
}
