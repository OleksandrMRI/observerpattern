package ua.shpp.abstracts;

public interface Observer {
    void handleEvent(int temperature, int pressure);
}
